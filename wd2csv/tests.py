from django.test import TestCase, Client
from django.urls import reverse

from wd2csv.views import sparql_query

# Create your tests here.


class WD2CSVHomepageTest(TestCase):
    def test_view_index(self):
        url = reverse("wd2csv_index")
        response = self.client.get(url)
        self.assertTemplateUsed(response, "wd2csv/index.dtl")


class WD2CSVHomepageTestWithPost(TestCase):
    def test_view_index_with_post(self):
        form_data = {
            "qids": "Q42921754",
            "pids": "P31",
            "languages": "en",
            "return_labels_for_properties": True,
            "return_labels_for_values": True,
        }
        c = Client()
        response = c.post("/wd2csv/", form_data)
        expected_result = b"item,label,description,instance of\r\nQ42921754,Sylvain Boissel,French Wikimedian,human\r\n"
        self.assertEqual(expected_result, response.content)


class WDZCSVSparqlQueryTest(TestCase):
    def test_sparql_query(self):
        sample_query = """
        SELECT ?item WHERE {
            ?item wdt:P31 wd:Q146.
        } ORDER BY ?item LIMIT 5
        """
        sample_result = "http://www.wikidata.org/entity/Q100965010"

        response = sparql_query(sample_query)

        self.assertIn(sample_result, str(response))