import csv
from django.http import HttpResponse
from django.shortcuts import render
from .forms import QueryForm
from SPARQLWrapper import SPARQLWrapper, JSON


def index(request):
    if request.method == "POST":
        form = QueryForm(request.POST)
        if form.is_valid():
            headers, rows = process_query(form.cleaned_data)
            return generate_csv(headers, rows)
    else:
        form = QueryForm()

    return render(request, "wd2csv/index.dtl", {"form": form})


def generate_csv(headers, rows):
    response = HttpResponse(content_type="text/csv")
    response["Content-Disposition"] = 'attachment; filename="response.csv"'

    writer = csv.DictWriter(response, fieldnames=headers)

    writer.writeheader()

    for key, row in rows.items():
        writer.writerow(row)

    return response


def sparql_query(query):
    endpoint = "https://query.wikidata.org/bigdata/namespace/wdq/sparql"
    sparql = SPARQLWrapper(endpoint, agent="ash-django")
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    return results["results"]["bindings"]


def get_entities(values, type="Q"):
    if type == "Q" or type == "L":
        prefix = "wd:"
    elif type == "P":
        prefix = "wdt:"
    else:
        raise ValueError("Entity type must be either L, Q or P")

    entities = []
    for v in values:
        if v[0].upper() == type and v[1:].isdigit():
            entities.append(prefix + v)
        else:
            error_text = "Please enter one {}id per line".format(type)
            raise ValueError(error_text)

    return entities


def process_query(data):
    items = get_entities(data["qids"], "Q")
    if len(data["languages"]):
        languages = "{}".format(", ".join(data["languages"]))
    else:
        languages = "en"

    if data["pids"]:
        properties = """
        VALUES ?direct {{
          {}
        }}
        """.format(
            "\n".join(get_entities(data["pids"], "P"))
        )
    else:
        properties = ""

    # Sample version of the query: http://tinyurl.com/y9oucmuw
    query = """
SELECT ?item ?itemLabel ?itemDescription ?prop ?propLabel ?value ?valueLabel
WHERE {{
  ?item ?direct ?value .
  ?prop wikibase:directClaim ?direct .

  VALUES ?item {{
  {}
  }}

  #Properties
  {}

  SERVICE wikibase:label {{ bd:serviceParam wikibase:language "{}" }}
}} ORDER BY ?item LIMIT 5000
    """.format(
        "\n".join(items), properties, languages
    )

    results = sparql_query(query)

    headers = ["item", "label", "description"]
    rows = {}

    for r in results:
        item = r["item"]["value"].split("/")[-1]

        if "itemLabel" in r:
            label = r["itemLabel"]["value"]
        else:
            label = ""

        if "itemDescription" in r:
            description = r["itemDescription"]["value"]
        else:
            description = ""

        if item not in rows:
            rows[item] = {"item": item, "label": label, "description": description}

        prop_id = r["prop"]["value"].split("/")[-1]
        prop_label = r["propLabel"]["value"]

        if data["return_labels_for_properties"]:
            prop = prop_label
        else:
            prop = prop_id

        if prop not in headers:
            headers.append(prop)

        value_raw = r["value"]["value"]
        value_label = r["valueLabel"]["value"]

        if data["return_labels_for_values"]:
            value = value_label
        else:
            if value_raw[0:31] == "http://www.wikidata.org/entity/":
                value_raw = value_raw[31:]
            value = value_raw

        rows[item][prop] = value

    return headers, rows
