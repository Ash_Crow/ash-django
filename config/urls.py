from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r"^admin/", admin.site.urls),
    url(r"^wd2csv/", include("wd2csv.urls")),
    url(r"^csv2qs/", include("csv2qs.urls")),
    url(r"", include("homepage.urls")),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        url(r"^__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
