# ash-django
Django apps for the Wikimedia Tool Labs, online at https://ash-django.toolforge.org

## Installation
- create django-config.ini in your home folder, based on django-config-sample.ini
- `git clone` the directory somewhere
- `cd ash-django`
- `python3 -m venv venv`
- `source venv/bin/activate`
- `pip3 install -r requirements.txt`
- `python manage.py migrate`
- `python manage.py collectstatic`
- `python manage.py runserver`
