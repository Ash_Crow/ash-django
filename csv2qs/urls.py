from django.conf.urls import url
from csv2qs import views as csv2qs_views

urlpatterns = [
    url(r"", csv2qs_views.index, name="csv2qs_index"),
]
