from django.apps import AppConfig


class Csv2QsConfig(AppConfig):
    name = "csv2qs"
