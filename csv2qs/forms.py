from django import forms


class UploadFileForm(forms.Form):
    title = forms.CharField(
        widget=forms.HiddenInput(), max_length=50, initial="csv_file"
    )
    # title field is mandatory so just filling and hiding it...
    file = forms.FileField()
