from django.test import TestCase, Client
from django.urls import reverse
from csv2qs.views import get_prop_types, strip_comments

from bs4 import BeautifulSoup as Soup

# Create your tests here.


class IndexViewNormalTest(TestCase):
    def test_view_index(self):
        url = reverse("csv2qs_index")
        response = self.client.get(url)
        self.assertTemplateUsed(response, "csv2qs/index.dtl")
        self.assertEqual(response.status_code, 200)


class IndexViewWithPostTest(TestCase):
    def test_valid_post_result(self):
        c = Client()
        with open("csv2qs/static/csv_sample.csv") as fp:
            response = c.post("/csv2qs/", {"file": fp, "title": "csv_file"})

        valid_response = """<pre contenteditable="true" id="commands">Q4115189\tLfr\t"Bac\\, sable"
Q4115189\tLen\t"Sandbox item"
Q4115189\tP31\tQ1\tS143\tQ48183\tS813\t+2018-10-28T00:00:00Z/9\tP580\t+00000001840-01-01T00:00:00Z/11\tP19\tQ22
Q4115189\tP625\t@043.26193/010.92708\tS143\tQ48183\tS813\t+2018-10-28T00:00:00Z/9
Q4115189\tP178\tQ42\tS143\tQ48183\tS813\t+2018-10-28T00:00:00Z/9\tP1082\t1111\tP2044\t42~643U11573
Q4115189\tP215\t"GaBuMeuhZo"\tS143\tQ48183\tS813\t+2018-10-28T00:00:00Z/9
Q4115189\tP1559\tfr:"Bien le bonjour"\tS143\tQ48183\tS813\t+2018-10-28T00:00:00Z/9
CREATE
LAST\tLfr\t"Un nouvel objet | de test"
LAST\tLen\t"A new test item"
LAST\tAfr\t"Test 1"
LAST\tAfr\t"Test 2"
LAST\tDfr\t"Test crÃ©ation objet"
LAST\tP31\tQ5
LAST\tSfrwiki\t"Test"</pre>"""
        soup = Soup(response.content, "html.parser")
        commands = str(soup.find(id="commands"))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(valid_response, commands)

    def test_invalid_post_result(self):
        c = Client()
        with open("csv2qs/tests/csv_sample_invalid.csv") as fp:
            response = c.post("/csv2qs/", {"file": fp, "title": "csv_file"})

        soup = Soup(response.content, "html.parser")
        response_warning = str(soup.find_all("div", "alert-warning")[0])
        expected_warning = (
            "s813 expects a Time but +2018-101-28T00:00:00Z/9 does not match the format"
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn(expected_warning, response_warning)


class GetPropTypesTest(TestCase):
    def test_get_prop_types(self):
        props = ["P31", "P2044"]

        response = get_prop_types(props)
        self.assertEqual(response["P31"], "WikibaseItem")
        self.assertEqual(response["P2044"], "Quantity")


class StripCommentsTest(TestCase):
    def test_strip_comments_with_pipe(self):
        response = strip_comments(" A | B")
        self.assertEqual(response, "A")

    def test_strip_comments_without_pipe(self):
        response = strip_comments("A B ")
        self.assertEqual(response, "A B")
