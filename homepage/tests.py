from django.test import TestCase
from django.urls import reverse

# Create your tests here.


class HomepageTest(TestCase):
    def test_view_index(self):
        url = reverse("index")
        response = self.client.get(url)
        self.assertTemplateUsed(response, "homepage/index.dtl")
